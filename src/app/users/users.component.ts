import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
    styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; } 
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50; }
  `]
})

export class UsersComponent implements OnInit {
users = [
     {name:'johnWall',email:'john@gmail.com'},
     {name:'Jack',email:'jack@gmail.com'},
     {name:'Alice',email:'alice@yahoo.com'}
   ]

currentUser = this.users[1];

select(user){
		this.currentUser = user; 
    console.log(	this.currentUser);
 }

  constructor() { }

  ngOnInit() {
  }

}
