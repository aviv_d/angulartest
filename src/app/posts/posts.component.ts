import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';

@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
  styleUrls: [`
    .posts li { cursor: default; }
    .posts li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class PostsComponent implements OnInit {
  
  posts;

  currentPost;

  isLoading = true;


 select(post){
    this.currentPost = post; 
    console.log(	this.currentPost);
    //console.log(post);
 }
  
  deletePost(post){
    this.posts.splice(
      this.posts.indexOf(post),1
    )
  }
  addPost(post){
    this.posts.push(post)
  }

  editUser(originalAndEdited){
    this.posts.splice(
      this.posts.indexOf(originalAndEdited[0]),1,originalAndEdited[1]  
    )
    console.log(this.posts);
  }  

  constructor(private _postsService: PostsService) {
  //  this.posts = this._postsService.getPosts();   לוקח נתנונים מהסרוויס(מקומי)
  }
  ngOnInit() {
    this._postsService.getPosts()
		    .subscribe(posts => {this.posts = posts;
                                    this.isLoading = false});

}
  

}
