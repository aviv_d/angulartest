import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { PostsComponent } from './posts/posts.component';
import {SpinnerComponent} from './shared/spinner/spinner.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';


import { PostsService } from './posts/posts.service';
import { UserComponent } from './user/user.component';
import { PostComponent } from './post/post.component';
import { PostFormComponent } from './post-form/post-form.component';

const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'posts', component: PostsComponent },
  { path: '', component: UsersComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    PostsComponent,
    PostFormComponent,
    UserComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    PostFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes)
  ],
 
  providers: [PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
